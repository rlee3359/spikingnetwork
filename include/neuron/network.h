#ifndef NETWORK_H
#define NETWORK_H

#include "neuron.h"
#include "connection.h"
#include <stdlib.h>
#include <list>

class Network {

    std::list<Neuron> neurons;
    std::list<Connection> connections;
    std::vector<int> spikeTrain;

    // std::vector<Output> outputs;
    // std::vector<std::vector<double> > neuronVoltages;
    // std::vector<std::vector<double> > neuronSpikes;

    int numInputNeurons, numHiddenNeurons, numOutputNeurons;
    
    public:
        Network();
        void buildNetworkFromParent(Network parent);
        void buildNetwork(int inputLayers, int hiddenLayers, int outputLayers);
        void connectNetwork();
        void step();

        void print();
        bool legalConnection(Neuron &source, Neuron &destination);
        bool connectionExists(std::list<Neuron>::iterator ns, std::list<Neuron>::iterator nd);
        void addNeuron();
        void removeNeuron();
        void addConnection();
        void mutateConnection();
        void mutateConnectionWeights(double mu);
        void removeConnection();
        void mutate();
        double getFitness();


        void reset();

        void Store();
        void SaveData(int parentNum);
        int fixedInput;
};

#endif
