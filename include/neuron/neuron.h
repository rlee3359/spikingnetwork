#ifndef NEURON_H 
#define NEURON_H 
#include <vector>
#include "output.h"

class Neuron {
    // Input parameters
    double a, b, c, d;
    // std::vector<double> pwmWindow;
    // Membrane variables
    // v -> Membrane potential
    // u -> Membrane recovery variable
    double v, u;
    double input;
    int layer;

    double spikeThreshold;
    int spike;

    int num;

    // Output output;

    // Private Members
    void PrintStats();

    // Public Methods
    public:
    // Constructor - Takes parameters
    Neuron(double, double, double, double);
    Neuron(double, double, double, double, int, int);
    void Print();
    void setInput(double i);
    void resetInput();
    void reset();
    void incrementInput(double i);

    void sendOutput();

    // Step the neuron forward one timestep
    void Step();

    // Return the voltage 
    double getV() const;
    int getSpike() const;
    void setLayer(int num);
    int getLayer() const;
    void setNum(int n);
    int getNum() const;

    double getA() const;
    double getB() const;
    double getC() const;
    double getD() const;
    // int setType(int newType);
    std::vector<double> voltageTrain;
    std::vector<int> spikeTrain;


};

#endif 
