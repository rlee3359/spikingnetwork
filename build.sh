echo "Clearing Build Directory"
rm -r ./build
mkdir build

echo "Building Project"
cd build
cmake ..
make

