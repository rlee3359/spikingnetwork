import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as patches
import math

class Neuron:
    def __init__(self, num, layer):
        self.layer = layer
        self.num = num

    def set_x(self):
        self.x = 5 * self.layer 
    def set_y(self, y):
        self.y = y

voltages = pd.read_table('network_output/voltage_output_0.txt', ' ')
spikes = pd.read_table('network_output/spike_output_0.txt', ' ')

network_file = open("network_output/topology_0.txt", "r")

neuron_counts = [int(n) for n in network_file.readline().strip('\n').split(',')]
print(neuron_counts)
print(len(voltages))

lines = network_file.readlines()
connections = lines[1:]
connections = [c.strip("\n").split(",") for c in connections]
connections = [list(map(float, c)) for c in connections]
print(connections)

voltage_lines = open("network_output/voltage_output_0.txt", "r").readlines()
print(voltage_lines[0])

neuron_headers = voltage_lines[0].strip("\n").split(" ")
neuron_indices = [tuple(n.split("l")) for n in neuron_headers]
neuron_indices = neuron_indices[0:-1]
neuron_indices = [(int(n[0]), int(n[1])) for n in neuron_indices]
print(neuron_headers)
print(neuron_indices)

neuron_list = []

for n in neuron_indices:
    neuron_list.append(Neuron(n[0], n[1]))

input_neurons = []
output_neurons = []
hidden_neurons = []
for n in neuron_indices:
    if n[1] == 0:
        input_neurons.append(n)
    elif n[1] == 1:
        hidden_neurons.append(n)
    elif n[1] == 2:
        output_neurons.append(n)

print(input_neurons)
print(hidden_neurons)
print(output_neurons)

ik = 0
hk = 0
ok = 0
for n in neuron_list:
    n.set_x()
    print(n.x)
    if n.layer == 0:
        n.set_y(ik-((neuron_counts[0]-1)/2))
        ik += 1
    elif n.layer == 1:
        n.set_y(hk-((neuron_counts[1]-1)/2))
        hk += 1
    elif n.layer == 2:
        n.set_y(ok-((neuron_counts[2]-1)/2))
        ok += 1
#
# input_x = [5*n[1] for n in input_neurons]
# input_y = [i*2 for i,n in enumerate(input_neurons)]
# hidden_x = [5*n[1] for n in hidden_neurons]
# hidden_y = [i-((len(hidden_neurons)-1)/2) for i,n in enumerate(hidden_neurons)]
# output_x = [5*n[1] for n in output_neurons]
# output_y = [i*2 for i,n in enumerate(output_neurons)]
#
#
# input_all = np.array([np.asarray(input_x).T, np.asarray(input_y).T, np.asarray(input_neurons).T])
# hidden_all = np.array([np.asarray(hidden_x).T, np.asarray(hidden_y).T, np.asarray(hidden_neurons).T])
# output_all = np.array([np.asarray(output_x).T, np.asarray(output_y).T, np.asarray(output_neurons).T])
#
# all_all = np.vstack((input_all, hidden_all, output_all))
# plt.ylim([-1, neuron_counts[1]])

fig1 = plt.figure(1)

i = 1
# voltages.drop(voltages.columns[len(voltages.columns)-1], axis=1, inplace=True)
L = len(voltages.columns)
# print(L)
print(voltages.head())
for neuron in voltages:
    if 'l' in neuron:
        print(neuron)
        layer = round(float(neuron.split('l')[1]))
        print("Layer",layer)

        plt.subplot(L, 1, i)
        title = ''
        color = ''
        if int(layer) == 0:
            title = "Input"
            frame1 = plt.gca()
            frame1.axes.get_xaxis().set_visible(False)
            frame1.axes.get_yaxis().set_visible(False)

            color = 'g'
        elif int(layer) == 1:
            title = "Hidden Layer"

            frame1 = plt.gca()
            frame1.axes.get_xaxis().set_visible(False)
            frame1.axes.get_yaxis().set_visible(False)

            color = 'b'
        elif int(layer) == 2:
            title = "Output Layer"
            color = 'r'
            plt.xlabel('Time Steps', fontsize=8)


        if i == math.ceil(sum(neuron_counts)/2):
            print(i)
            plt.ylabel('Membrane Potential (mV)', fontsize=8)

        # print(voltages[neuron])
        voltages[neuron].plot(linewidth=0.7, color=color)
        # sp = spikes[neuron]
        # sp[sp == 0] = -100
        s = spikes[neuron][spikes[neuron] == 1]
        plt.plot(s,'k+', ms=3)

        # plt.title(title, fontsize = 5)
        # spikes[neuron].plot();
        i += 1
#
# offset = (math.ceil(neuron_counts[1]/3)) - 1
# iy = [y+offset for y in range(0,neuron_counts[0])]
# ix = [5 for x in range(0,neuron_counts[0])]
# # print(ix, iy)
#
# hy = [y for y in range(0,neuron_counts[1])]
# hx = [15 for x in range(0,neuron_counts[1])]
#
# oy = [y+offset for y in range(0,neuron_counts[2])]
# ox = [25 for x in range(0,neuron_counts[2])]
#
#
#
# neuron_x = ix + hx + ox
# neuron_y = iy + hy + oy
#
f2 = plt.figure(2)
ax = f2.add_subplot(111, aspect='equal')

frame1 = f2.gca()
frame1.axes.get_xaxis().set_visible(False)
frame1.axes.get_yaxis().set_visible(False)


for c in connections:
    s = int(c[0])
    d = int(c[2])

    for n in neuron_list:
        if n.num == s:
            ys = n.y
            xs = n.x
        if n.num == d:
            yd = n.y
            xd = n.x
    
    x = [xs, xd]
    y = [ys, yd]

    print(c, x, y)
    if c[4] > 1:
        c[4] = 1
    if c[4] < 0:
        c[4] = 0
    clr = (1, c[4], c[4])
    print(clr)
    if c[5] > 0:
        clr = (c[4], c[4], 1)

    if x[0] == x[1] and y[0] == y[1]:
        p = patches.FancyArrowPatch((x[0], y[0]), (x[1]+0.01, y[1]+0.01), arrowstyle = '->', connectionstyle = 'arc3, rad=3', color = clr, mutation_scale=20)
        ax.add_patch(p)
    elif x[0] == x[1]:
        p = patches.FancyArrowPatch((x[0], y[0]), (x[1], y[1]), arrowstyle = '->', connectionstyle = 'arc3, rad=0.2', color = clr, mutation_scale=20)
        ax.add_patch(p)
    else:
        plt.plot(x, y, color = clr, linewidth=1) #str(c[2]))

for n in neuron_list:
    color = 'ko'
    if n.layer == 0:
        color = 'go'
    elif n.layer == 1:
        color = 'bo'
    elif n.layer == 2:
        color = 'ro'
    plt.plot(n.x, n.y, color, ms = 10, markerfacecolor='w')

plt.ylim([-max(neuron_counts)/2, max(neuron_counts)/2])
plt.show()
