#include <iostream>
#include <neuron/neuron.h>
#include <neuron/network.h>
#include <math.h>
#include <ctime>
#include <unistd.h>
#include <iostream>
#include <fstream>

#define RAND_WEIGHT (rand() / (RAND_MAX + 1.))

void saveFitnesses(std::vector<double> fitnesses);
void hillClimber();
void parralelHillClimber();
void test();
void pulseWidthTest();

int main(int argc, char **argv) {
    //Seed rand
    srand(time(NULL));

    
    // if (RAND_WEIGHT < sensorNormalized) {
    //     spike = 1;
    // } else {
    //     spike = 0;
    // }


    // hillClimber();
    // parralelHillClimber();
    // test();
    pulseWidthTest();

    return 0;
}

void saveFitnesses(std::vector<double> fitnesses) {
    std::ofstream fitnessFile;
    std::string fitnesssFileName = "network_output/fitnesses.txt";
    fitnessFile.open(fitnesssFileName);

    for (auto f = fitnesses.begin(); f != fitnesses.end(); ++f) {
        fitnessFile << *f << ",";
    }

    fitnessFile.close();

    return;
}


//fill popuation up to 20
//evaluate all 20
//
////generation start
//in each gen
//for (net in population)
//  create 4 children, mutate,  and get their fitness
//  we have 5 possible parents, pick the fittest )(may be previous parent)
//  add the best to that population slot


void parralelHillClimber() {
    int numOfGens = 100;
    int numChildren = 4;
    int populationSize = 20;

    std::vector<Network> population;
    for (int p = 0; p != 20; p++) {
        population.push_back(Network());
        population[p].buildNetwork(2,5,1);
        population[p].connectNetwork();
        population[p].fixedInput = 10;
    }

    // All generations
    for (int currentGen = 0; currentGen < numOfGens; currentGen++) {
        std::vector<double> fitnesses (populationSize, 0);
        // For all parent networks
        for (auto parent = population.begin(); parent != population.end(); ++parent) {
            parent->reset();
            // Create new children vector and fitness vector 
            std::vector<Network> children;
            // std::vector<double> fitnesses;

            // Add parent fitness first
            double parentFitness = (parent->getFitness());

            // std::cout << "Parent fitness: " << parentFitness << "\n   Children: "<< std::endl;
            double bestChildFitness = 100000;
            int bestChild;
            // Create a new set of children, mutate 
            for (int ch = 0; ch != numChildren; ch++) {
                children.push_back(Network());
                children[ch].buildNetworkFromParent(*parent);
                children[ch].mutate();
                children[ch].reset();

                // Get the best child
                double childFitness = children[ch].getFitness();
                std::cout << childFitness << std::endl;
                if (childFitness < bestChildFitness) {
                    bestChild = ch;
                    bestChildFitness = childFitness;
                }
            }

            if (bestChildFitness < parentFitness) {
                *parent = Network();
                parent->buildNetworkFromParent(children[bestChild]);
                fitnesses[std::distance(population.begin(), parent)] = bestChildFitness;
            } else {
                fitnesses[std::distance(population.begin(), parent)] = parentFitness;
            }
        }

        // SAVE THE CURRENT FITTEST PARENTS TO THE FILE
        // std::vector<double> finalFitnesses;
        saveFitnesses(fitnesses);

        // int fittestIndex;
        // double bestFitness = 100000;
        for (int p = 0; p != populationSize; p++) {
            // population[p].reset();
            // double thisFitness = population[p].getFitness();
            // if (thisFitness < bestFitness) {
            //     bestFitness = thisFitness;
            //     fittestIndex = p;
            // }
            // finalFitnesses.push_back(thisFitness);
            population[p].SaveData(p);
        }

        // sleep(5);
        // population[fittestIndex].print();
        // DONE
    }
    // Left with a vector of the fittest parents
    std::vector<double> finalFitnesses;

    int fittestIndex;
    double bestFitness = 100000;
    for (int p = 0; p != populationSize; p++) {
        population[p].reset();
        double thisFitness = population[p].getFitness();
        if (thisFitness < bestFitness) {
            bestFitness = thisFitness;
            fittestIndex = p;
        }
        finalFitnesses.push_back(thisFitness);
    }

    std::cout << "Final fitness: " << bestFitness << std::endl;

    population[fittestIndex].print();
    population[fittestIndex].SaveData(20);
}


void hillClimber() {

    std::ofstream fitnessFile;
    std::string fitnesssFileName = "network_output/fitness_hill.txt";
    fitnessFile.open(fitnesssFileName);


    Network parent = Network();
    parent.buildNetwork(1,7,1);
    parent.connectNetwork();
    parent.fixedInput = 10;


    int numOfGens = 1000;
    for (int currentGen = 0; currentGen < numOfGens; currentGen++) {
        // Reset parent
        parent.reset();
        // Create new child from parent
        Network child = Network();
        child.buildNetworkFromParent(parent);

        // Mutate Child
        child.mutate();

        // Get the fitness of each - run through timesteps
        double childFitness = child.getFitness();
        double parentFitness = parent.getFitness();
        std::cout << "Fitness: " << parentFitness << std::endl;
        // std::cout << "Child Fitness:" << childFitness << " Parent Fitness:" << parentFitness << std::endl;
        if (childFitness < parentFitness) {
            // std::cout << "Parent replaced\n";
            parent = Network();
            parent.buildNetworkFromParent(child);
            saveFitnesses(std::vector<double> (1, childFitness));
            fitnessFile << childFitness << ",";
        } else {
            saveFitnesses(std::vector<double> (1, parentFitness));
            fitnessFile << parentFitness << ",";
        }
        parent.SaveData(0);
        // sleep(1);
    }

    fitnessFile.close();

    // parent.reset();
    std::cout << "FITTEST PARENT DEMONSTRATION" << std::endl;

    // while (1) {
    //     parent.step();
    // }
}

void pulseWidthTest() {
    std::vector<int> sizes = {1, 5, 10, 50, 100, 250, 500, 750, 1000};

    // Create networks of different sizes and step through them
    for (auto s = sizes.begin(); s != sizes.end(); ++s) {
        // Create the new network!
        Network testNet = Network();
        testNet.buildNetwork(1, *s, 1);
        testNet.connectNetwork();
        testNet.fixedInput = 10;

        std::cout << "Testing network with " << *s << " hidden neurons." << std::endl;
        // Step through for 1000 timesteps
        for (int ts = 0; ts < 1000; ts++) {
            testNet.step();
        }
        sleep(2);

    }
}


void test() {
    // Initialize neuron
    // Neuron neuron = Neuron(0.02, 0.2, -65.0, 8.0);

    // Create a network
    Network nn = Network();
    nn.buildNetwork(2, 3, 1);
    nn.connectNetwork();
    // nn.removeConnection();
    // nn.mutateConnectionWeights(0.5);
    // nn.addConnection();
    // nn.addNeuron();
    // nn.removeNeuron();

    int cnt = 0;

    // Loop - generate signal
    while (1) {
        // child.step();
        // std_msgs::Float64 v = std_msgs::Float64();
        // v.data = nn.v;

        // Output voltage of one of the hidden neurons
        // voltagePub.publish(v);
        //
        nn.step();

        cnt ++;

        // child.fixedInput = floor(cnt/100) * 5;
        // std::cout << nn.fixedInput << std::endl;

        if (cnt > 300) {
            nn.SaveData(0);
            // std::cout << "FILE STORED! \n \n \n \n";
            return;
        }
    }
}

