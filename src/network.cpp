#include <neuron/network.h>
#include <neuron/neuron.h>
#include <iostream>
#include <fstream>
#include <numeric>
#include <list>
#include <iterator>

#define RAND_TYPE (((rand() % 2) - 1) == 0) ? 50 : -50
#define RAND_WEIGHT (rand() / (RAND_MAX + 1.))
#define NEURON_PROB 0.1
#define CONN_PROB 0.2
// #define RAND_TYPE 1

Network::Network() {
    // buildNetwork(1, 1, 1);
}

void Network::buildNetworkFromParent(Network parent) {
    // Create an exact duplicate of the parent network from scratch!
    // Get layer counts
    numInputNeurons = parent.numInputNeurons;
    numHiddenNeurons = parent.numHiddenNeurons;
    numOutputNeurons = parent.numOutputNeurons;
    // First build network with same number of neurons
    for (auto n = parent.neurons.begin(); n != parent.neurons.end(); ++n) {
        // Manually pull every parameter and attribute from each neuron
        neurons.push_back(Neuron(n->getA(), n->getB(), n->getC(), n->getD(), n->getLayer(), n->getNum()));
        // Add containers for storage
        // neuronVoltages.push_back(std::vector<double>());
        // neuronSpikes.push_back(std::vector<double>());
    }
    // Now connect them in the same way
    for (auto pc = parent.connections.begin(); pc != parent.connections.end(); ++pc) {
        double weight = pc->weight;
        int type = pc->type;
        std::list<Neuron>::iterator source;
        std::list<Neuron>::iterator destination;
        for (auto n = neurons.begin(); n != neurons.end(); ++n) {
            if (n->getNum() == pc->source->getNum()) {
                source = n;
            }
            if (n->getNum() == pc->destination->getNum()) {
                destination = n;
            }
        }
        connections.push_back(Connection(source, destination, weight, type));
    }
    fixedInput = parent.fixedInput;
}

// Contruct the network - Counts the total neurons and fills a vector of standard neurons
void Network::buildNetwork(int numInput, int numHidden, int numOutput) {
    // This implementation assumes all neurons have the same parameters (a,b,c,d)
    // Otherwise, revert back to three for loops with different params for i,h,o
    // Sum number of neurons to create all neurons
    numInputNeurons = numInput;
    numHiddenNeurons = numHidden;
    numOutputNeurons = numOutput;
    int totalNeurons = numInputNeurons + numHiddenNeurons + numOutputNeurons;

    // Create all neurons
    for (int i = 0; i < totalNeurons; i++) {
        // Get layer number
        int layer;
        if (i < numInputNeurons) {
            layer = 0;
        } else if (i < numInputNeurons + numHiddenNeurons) {
            layer = 1;
        } else {
            layer = 2;
        }
        neurons.push_back(Neuron(0.1, 0.25, -55, 4, layer, i));

        // Create containers for saving voltages to file
        // neuronVoltages.push_back(std::vector<double>());
        // neuronSpikes.push_back(std::vector<double>());
    }
}

void Network::reset() {
    for (auto n = neurons.begin(); n != neurons.end(); ++n) {
        n->reset();
    }
}

void Network::print() {
    std::cout << "Connections size: " << connections.size() << std::endl;
    std::cout << "Neurons size: " << neurons.size() << std::endl;
    for (auto c = connections.begin(); c != connections.end(); ++c) {
        std::cout << "S:" << c->source->getLayer() << " #" << c->source->getNum();
        std::cout << " D:" << c->destination->getLayer() << " #" << c->destination->getNum();
        std::cout << " W:" << c->weight;
        std::cout << " T:" << c->type << std::endl;
    }
}

// Create the network connections - Fills a vector of connections, connects all hidden neurons to eachother and inputs and outputs
// Recurrent connections are included, as well as connections between each hidden neuron
void Network::connectNetwork() {
    std::cout << "Connecting Network!" << std::endl;
    
    // Connect all neurons!
    for (auto ni = neurons.begin(); ni != neurons.end(); ++ni) {
        for (auto no = neurons.begin(); no != neurons.end(); ++no) {
            if (legalConnection(*ni, *no)) {
                double weight = RAND_WEIGHT;
                int type = RAND_TYPE;
                std::list<Neuron>::iterator nit = ni;
                connections.push_back(Connection(ni, no, weight, type));
            }
        }
    }
    //
    // // Print what was constructed
    // std::cout << "Neuron count: " << neurons.size() << std::endl;
    // std::cout << "Connection count: " << connections.size() << std::endl;
    // std::cout << "Connections: \n"; 
    // //
    // // Print all connection params - for debugging
    // for (auto c = connections.begin(); c != connections.end(); ++c) {
    //     std::cout << "  Connection " << "\n" << "Src layer: " << c->source->getLayer()
    //         << "\n      Dst layer: " << c->destination->getLayer() << "\nWeight: " << c->weight
    //         << "\n      Type: " << c->type << std::endl;
    // }
}

bool Network::legalConnection(Neuron &source, Neuron &destination) {
	if(source.getLayer()==2 || destination.getLayer()==0){
		return false;
	}
	if(source.getLayer()==0 && destination.getLayer()==2){
		return false;
	}
	return true;
}



// Step through the network
void Network::step() {

    // std::cout << "STEPPING\n";
    // Apply input to input neurons
    for (auto i = neurons.begin(); i != neurons.end(); ++i) {
        // Reset input to all neurons, set constant input to input layer
        i->resetInput();
        if (i->getLayer() == 0) {
            i->setInput(fixedInput); 
        }
    }

    // Apply all spikes through connections (increment neuron's input)
    for (auto c = connections.begin(); c != connections.end(); ++c) {
        double input = c->source->getSpike() * c->weight * c->type;
        c->destination->incrementInput(input);
    }

    // std::cout << "Stepping with inputs\n";
    // Finally, step all neurons
    for (auto n = neurons.begin(); n != neurons.end(); ++n) {
        n->Step();
    }
    for (auto n = neurons.begin(); n != neurons.end(); ++n) {
        if (n->getLayer() == 2) {
            n->sendOutput();
        }
    }
    
    // STORING IS NOW DONE INSIDE NEURON
    // Store();
}

void Network::Store() {
    // int num = 0;
    // for (auto n = neurons.begin(); n != neurons.end(); ++n) {
    //     neuronVoltages[num].push_back(n->getV());
    //     neuronSpikes[num].push_back(n->getSpike());
    //     num++;
    // }
}

void Network::SaveData(int parentNum) {
    std::ofstream voltageFile;
    std::ofstream spikeFile;
    std::ofstream topologyFile;

    std::string voltageOutputName = "network_output/voltage_output_" + std::to_string(parentNum) + ".txt";
    std::string spikeOutputName = "network_output/spike_output_" + std::to_string(parentNum) + ".txt";
    std::string topologyName = "network_output/topology_" + std::to_string(parentNum) + ".txt";


    voltageFile.open(voltageOutputName);
    spikeFile.open(spikeOutputName);
    topologyFile.open(topologyName);

    topologyFile << numInputNeurons << "," << numHiddenNeurons << "," << numOutputNeurons << "\n";
    topologyFile << "Connections s,l,d,l,w,t\n";

    for (auto c = connections.begin(); c != connections.end(); ++c) {
         Connection con = *c;
         topologyFile << con.source->getNum() << "," << con.source->getLayer() << "," << con.destination->getNum() << "," << con.destination->getLayer() << "," << con.weight << "," << con.type << "\n"; 
    }

    topologyFile.close();


    // Save neuron numbers
    for (auto n = neurons.begin(); n != neurons.end(); ++n) {
        std::string space = " ";

        int num = n->getNum();
        voltageFile << num << "l" << n->getLayer() << space;
        spikeFile << num << "l" << n->getLayer() << space;
    }
    voltageFile << std::endl;
    spikeFile << std::endl;

    for (int i = 0; i != neurons.begin()->voltageTrain.size(); i++) {
        for (auto n = neurons.begin(); n != neurons.end(); ++n) {
            std::string space = " ";
            voltageFile << n->voltageTrain[i] << space; 
            spikeFile << n->spikeTrain[i] << space;
        }
        voltageFile << std::endl;
        spikeFile << std::endl;
    }

    voltageFile.close();
    spikeFile.close();
}

// MODULATE NEURAL NETWORK
// FOR GENETIC ALGORITHM

void Network::addNeuron() {
    std::cout << "Adding neuron" << std::endl;
    numHiddenNeurons++;
    int totalNeurons = numInputNeurons + numHiddenNeurons + numOutputNeurons;

    // totalNeurons as neuron number was wrong because network could shrink and then
    // numbers could be repeated!
    // Instead, use one larger than the largest neuron number! Numbers will
    // never be repeated, and new neurons will always have the highest number
    int largestId = 0; // There will always be an input neuron with 0 Id
    for (auto n = neurons.begin(); n != neurons.end(); ++n) {
        if (n->getNum() > largestId) {
            largestId = n->getNum();
        }
    }
    int newId = largestId + 1;
    std::cout << newId << std::endl;

    int layer = 1;
    neurons.push_back(Neuron(0.1, 0.25, -55, 4, layer, newId));
    std::list<Neuron>::iterator newNeuron = neurons.end();
    --newNeuron;

    for (auto no = neurons.begin(); no != neurons.end(); ++no) {
        if (legalConnection(*newNeuron, *no)) {
            if (RAND_WEIGHT > CONN_PROB) {
                double weight = RAND_WEIGHT;
                int type = RAND_TYPE;
                connections.push_back(Connection(newNeuron, no, weight, type));
            }
        }
        if (legalConnection(*no, *newNeuron) && no != newNeuron) {
            if (RAND_WEIGHT > CONN_PROB) {
                double weight = RAND_WEIGHT;
                int type = RAND_TYPE;
                connections.push_back(Connection(no, newNeuron, weight, type));
            }
        }
    }

    // Adding a storage vector for plotting
    // neuronVoltages.push_back(std::vector<double>());
    // neuronSpikes.push_back(std::vector<double>());

}

void Network::removeNeuron(){
    // Don't remove the last neuron!!
    if (neurons.size() <= numInputNeurons + numOutputNeurons + 1) {
        return;
    }

    int randNeuronNumber = rand() % numHiddenNeurons;
    int counter = 0;
    // Iterate over all neurons, count the hidden ones found until random chosen one found
    std::list<Neuron>::iterator toDelete;
    for (auto no = neurons.begin(); no != neurons.end(); ++no) {
        if (no->getLayer() == 1) {
            if (counter == randNeuronNumber) {
                toDelete = no;
                break;
            }
            counter++;
        }
    }

    // Vector of connections (iterator) that need to be deleted
    std::vector<std::list<Connection>::iterator> connsToDelete;
    for (auto c = connections.begin(); c != connections.end(); ++c) {
        if (c->source == toDelete || c->destination == toDelete) {
            // std::cout << "Connection deletion found \n";
            connsToDelete.push_back(c);
        }
    }

    // Erase all connections that need deleting
    // std::cout << "consdelete size" << connsToDelete.size();
    for (auto cd = connsToDelete.begin(); cd != connsToDelete.end(); ++cd) {
        // std::cout << "Removing connection" << std::endl;
        connections.erase(*cd); // Iterator to iterator to connection so dereference
    }

    // Erase neuron that has been chosen
    // std::cout << "Removing neuron\n";
    neurons.erase(toDelete);
    numHiddenNeurons--;
}


// // mutateConnection
// void Network::mutateConnection() {
//
//
// }
//


double Network::getFitness() {
    double fitness = 0;
    int numTimeSteps = 500;
    int spikeCount = 0;
    // for (int ts = 0; ts < numTimeSteps; ts++) {
    //     step();
    //     for (auto n = neurons.begin(); n !=neurons.end(); ++n) {
    //         if (n->getLayer() == 2) {
    //             if (n->getSpike() == 1) {
    //                 spikeCount++;
    //             }
    //         }
    //     }
    // }
    for (int ts = 0; ts < numTimeSteps; ts++) {
        step();
        for (auto n = neurons.begin(); n !=neurons.end(); ++n) {
            if (n->getLayer() == 2) {
                if (n->getSpike() == 1) {
                    spikeCount++;
                }
            }
        }
    }

    fitness = (double)spikeCount/(double)numTimeSteps;

    // // std::cout << spikeCount << " spike count out of 500\n";
    // fitness = (double)spikeCount/(double)numTimeSteps;
    // // std::cout << fitness << " of 500 spikes\n";
    // fitness = 1.0 - fitness;
     // std::cout << "Fitness: " << fitness << std::endl;
    return fitness;
}


void Network::mutate() {
    // Mutate weights every time - each connection has its own likelihood in the method
    mutateConnectionWeights(0.1);

    // Use the probabilities of each mutation
    // Probabilities of these functions is only handled here, not inside
    if (RAND_WEIGHT < NEURON_PROB) {
        // std::cout << "Adding a neuron\n";
        addNeuron();
    }
    if (RAND_WEIGHT < NEURON_PROB) {
        // std::cout << "Removing a neuron\n";
        removeNeuron();
    }
    if (RAND_WEIGHT < CONN_PROB) {
        // std::cout << "Adding a connection\n";
        addConnection();
    }
    if (RAND_WEIGHT < CONN_PROB) {
        // std::cout << "Removing a connection\n";
        removeConnection();
    }
}

void Network::mutateConnectionWeights(double mu) {
    // std::cout << "Mutating weights\n";
    for (auto c = connections.begin(); c != connections.end(); ++c) {
        if (RAND_WEIGHT < mu) {
            // Randomly increase or decrease by random double between 0 and 0.1
            if (RAND_WEIGHT < 0.5) {
                c->weight = c->weight + RAND_WEIGHT*0.1;
            } else {
                c->weight = c->weight - RAND_WEIGHT*0.1;
            }
        }
    }
}

void Network::addConnection() {
    // std::cout << "Adding connection\n";
    // Fill a vector with all possible connections
    std::vector<Connection> possibleConnections;
    for (auto ns = neurons.begin(); ns != neurons.end(); ++ns) {
        for (auto nd = neurons.begin(); nd != neurons.end(); ++nd) {
            if (legalConnection(*ns, *nd) and !connectionExists(ns, nd)) {
                if (nd != ns) {
                    // Add a possible connection - pre-fill with weight and type too
                    possibleConnections.push_back(Connection(ns, nd, RAND_WEIGHT, RAND_TYPE));
                }
            }
        }
    }
    if (possibleConnections.size() == 0) {
        // std::cout << "No possible connections found\n";
        return;
    }

    // Pick a random connection from list of possible options and add
    int randomConnectionIndex = rand() % possibleConnections.size();
    connections.push_back(std::move(possibleConnections[randomConnectionIndex]));
    // std::cout << connections.size() << std::endl;
}

void Network::removeConnection() {
    // If there are no connections, return
    if (connections.size() < 1) {
        return;
    } else if (connections.size() == 1) {
        connections.pop_back();
    } else {  // Remove a random connection
        auto connToRemove = connections.begin();
        std::advance(connToRemove, (rand() % connections.size()));
        connections.erase(connToRemove);
    }
}


bool Network::connectionExists(std::list<Neuron>::iterator ns, std::list<Neuron>::iterator nd) {
    // Check all connections for one where source and dest match input
    for (auto c = connections.begin(); c != connections.end(); ++c) {
        if (c->source->getNum() == ns->getNum() && c->destination->getNum() == nd->getNum()) {
            return true;
        }
    }
    return false;
}
