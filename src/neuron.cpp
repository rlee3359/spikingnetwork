#include <neuron/neuron.h>
#include <vector>
#include <iostream>

// Constructor - Create neuron with provided params
Neuron::Neuron(double aa, double bb, double cc, double dd) {
    // Neuron Parameters
    a = aa;
    b = bb;
    c = cc;
    d = dd;

    // Start dormant
    spike = 0;
    input = 0;
    // Membrane potetntial threshold
    spikeThreshold = 35;

    //Membrane Values
    v = c; // membrane potential
    u = b * v; // membrane reset value

    // pwmWindow = std::vector<double>();
    
    // Print fresh neuron
    PrintStats();
    Print();
}
// Constructor - Create neuron with provided params
Neuron::Neuron(double aa, double bb, double cc, double dd, int l, int num) {
    // Set layer
    setLayer(l);
    setNum(num);

    // Neuron Parameters
    a = aa;
    b = bb;
    c = cc;
    d = dd;

    // Start dormant
    spike = 0;
    input = 0;
    // Membrane potetntial threshold
    spikeThreshold = 35;

    //Membrane Values
    v = c; // membrane potential
    u = b * v; // membrane reset value

    // pwmWindow = std::vector<double>();
    
    // Print fresh neuron
    // PrintStats();
    // Print();
    //
    //
    
    //If output neuron, init output - maybe find a better way later
    if (layer == 2) {
        // output = Output();
    }
}

void Neuron::reset() {
    // Hard 
    // Reset spike and input
    spike = 0;
    resetInput();
   
    //Membrane Values
    v = c; // membrane potential
    u = b * v; // membrane reset value

    // pwmWindow.clear();
    spikeTrain.clear();
    voltageTrain.clear();
}


void Neuron::setNum(int n) {
    num = n;
}

int Neuron::getNum() const {
    return num;
}

// Print neuron values
void Neuron::Print() {
    std::cout << "Current Neuron Values:" << std::endl;
    std::cout << "v = " << v << std::endl;
    std::cout << "u = " << u << std::endl;
}

// Print neuron parameters
void Neuron::PrintStats() {
    //Message Displaying Neuron Info
        std::cout << "Izhikevich Neuron Object Created with: \n" <<
        "a = " << a << "\nb = " << b << "\nc = " << c 
        << "\nd = " << d << std::endl;
}


// Step through the simulation
// Update values
void Neuron::Step() {
    // std::cout << "Updating a neuron" << std::endl;
    // Update neuron values
    double vDelta = 0.04 * (v*v) + 5.0 * v + 140.0 - u + input;
    double uDelta = a * (b * v - u);

    v += vDelta;
    u += uDelta;

    // Spike if threshold met
    if (v >= spikeThreshold) {
        // std::cout << "Fired" << std::endl;
        spike = 1;
        v = c;
        u = u + d;
    } else {
        spike = 0;
    }

    spikeTrain.push_back(spike);
    voltageTrain.push_back(v);

    // Keep last 100 spikes
    // pwmWindow.push_back(spike);
    // if (pwmWindow.size() >= 100) {
    //     pwmWindow.erase(pwmWindow.begin());
    // }
}


// Get the neuron voltage
double Neuron::getV() const {
    return v;
}

// Get the neurons spike status
int Neuron::getSpike() const {
    return spike;
}

// Set the neuron input
void Neuron::setInput(double i) {
    input = i;
}

void Neuron::incrementInput(double i) {
    input += i;
}

void Neuron::resetInput() {
    input = 0;
}

void Neuron::setLayer(int num) {
    layer = num;
}
int Neuron::getLayer() const {
    return layer;
}

double Neuron::getA() const {
    return a;
}

double Neuron::getB() const {
    return b;
}

double Neuron::getC() const {
    return c;
}

double Neuron::getD() const {
    return d;
}

void Neuron::sendOutput() {
    // output.sendOutput(getSpike());
}
// void Neuron::setType(int newType) {
//     // type = newType;
// }
