#include "neuron/connection.h"
#include "neuron/neuron.h"
#include <list>
#include <iterator>

Connection::Connection() {
}

Connection::Connection(std::list<Neuron>::iterator s, std::list<Neuron>::iterator d, double w, int t) {
   // Set source, destination, weight
   source = s;
   destination = d;
   weight = w;
   // Type 1 for now (excitatory)
   type = t;
}

